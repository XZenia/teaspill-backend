const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Message = new Schema({
  message: {
      type: String
  },
  username:{
      type: String
  },
  room:{
      type: String
  },
  date: {
      type: Date
  },
});

module.exports = mongoose.model('Message', Message);
