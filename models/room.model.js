const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Room = new Schema({
  id: {
      type: String
  },
  name:{
      type: String
  },
  created_at: {
      type: Date
  },
  updated_at: {
      type: Date
  }
});

module.exports = mongoose.model('Room', Room);
