const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let User = new Schema({
  id: {
      type: String
  },
  room: {
      type: String
  }
});

module.exports = mongoose.model('User', User);
