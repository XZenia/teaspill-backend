var app = require('express')();

var http = require('http').createServer(app);

const cors = require('cors');
const express = require('express');

//MongoDB dependencies
const mongoose = require('mongoose');

const { v4: uuidv4 } = require('uuid');
var cookieParser = require('cookie-parser');
var methodOverride = require('method-override');

mongoose.connect('mongodb://127.0.0.1:27017/teaspill', {useNewUrlParser: true, useUnifiedTopology: true});

const port = 3000;

let Message = require('./models/message.model');
let Room = require('./models/room.model');
let User = require('./models/user.model');

app.use(express.json());
app.use(methodOverride());
app.use(cookieParser());
app.use(cors());

//Room routes.
app.get('/room/:id', (req, res) =>
  Room.findOne({id: req.params.id}, function(err, room) {
    res.send(room);
  })
);

app.get('/room/messages/:id', (req, res) =>
  Message.find({room: req.params.id}, function(err, messages) {
    var messageList = [];
    if (messages){
      messages.forEach(function(message) {
        messageList.push(message);
      });
    }
    res.json(messageList);
  })
);

app.get('/rooms', (req, res) =>
  Room.find({}, function(err, rooms) {
    var roomList = [];

    rooms.forEach(function(roomData) {
      roomList.push(roomData);
    });

    res.send(JSON.stringify(roomList));
  })
);

app.post('/create-room', (req, res) => {
  let room = new Room();
  room.name = req.body['roomName'];
  room.id = uuidv4();
  room.created_at = new Date();
  room.updated_at = new Date();
  room.save().then(newRoom => {
      res.status(200).json({'message': 'Room created successfully.', 'code': 200, 'room': newRoom});
  }).catch(err => {
      res.status(400).json({'message': err, 'code': 400});
  });
});


http.listen(port, () => {
  console.log('Currently listening on port '+port);
});

var io = require('socket.io')(http, {
  cors: {
    origin: '*',
  }
});

io.on('connection', (socket) => {
    socket.on('send-message', (msg) => {
        var messageData = JSON.parse(msg);
        let newMessage = new Message();
        newMessage.username = socket.id;
        newMessage.message = messageData.message;
        newMessage.room = messageData.roomId;
        newMessage.date = new Date();

        newMessage.save().then(newRoom => {
            var data = {
              'username':newMessage.username,
              'message':newMessage.message,
              'room': newMessage.room,
              'date': newMessage.date
            };
            socket.broadcast.emit('message-broadcast', JSON.stringify(data));
        }).catch(err => {
            res.status(400).send('Sending new message failed!');
        });

    });

    socket.on("join room", roomID => {
      socket.join(roomID);
      socket.to(roomID).emit("other user", socket.id);
      socket.broadcast.to(roomID).emit('user joined', socket.id);
      socket.emit("current user", socket.id);
    });

    socket.on("offer", (payload) => {
        console.log("Offer received!");
        console.log(payload);
        socket.to(payload.target).emit("offer", payload);
    });

    socket.on("answer", (payload) => {
        console.log("Answer received!");
        console.log(payload);
        socket.to(payload.target).emit("answer", payload);
    });

    socket.on("ice-candidate", (incoming) => {
        console.log("ICE Candidate received!");
        console.log(incoming);
        socket.to(incoming.target).emit("ice-candidate", incoming.candidate);
    });
});



